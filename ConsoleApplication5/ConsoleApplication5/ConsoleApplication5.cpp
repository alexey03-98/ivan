#include    "stdafx.h"
#include    <iostream>
#include    <chrono>
#include    <utility>
#include    <vector>
#include    <string>
#include    <sstream>


typedef std::pair<double, double> vertex;
typedef std::vector<vertex>     Vertices;


class ICE {
private:
	double   I{ 10 };
	double   V{ 0 };
	double   T_max{ 110 };
	double   T;
	double   Hm{ 0.01 };
	double   Hv{ 0.0001 };
	double   C{ 0.1 };
	//{ 20, 75, 100, 105, 75, 0 }, { 0, 75, 150, 200, 250, 300 };
	Vertices    MV{ { 0, 20 },{ 75, 75 },{ 150, 100 },{ 200, 105 },{ 250, 75 },{ 300, 0 } };
	double   mv_relation(double v) {
		double   k = 0;
		double   b = 0;

		for (int i = 0; i < MV.size() - 1; i++) {
			if (v <= MV[i + 1].first && v >= MV[i].first) {
				k = (MV[i + 1].second - MV[i].second) / (MV[i + 1].first - MV[i].first);
				b = MV[i].second - k * MV[i].first;

				return k * v + b;
			}
		}

		return 0.0f;
	}

	void    one_tact(double elapsedTime, double T0) {
		double   M = mv_relation(V);
		double   a = M / I;
		double   Vh = M * Hm + V * V*Hv;
		double   Vc = C * (T - T0);

		V += elapsedTime * a;
		T += (Vh - Vc)*elapsedTime;
	}

public:
	ICE() {}
	ICE(double i, double t_max, double h_m, double h_v, double c) : I(i), T_max(t_max), Hm(h_m), Hv(h_v), C(c) {}
	ICE(const ICE &engine) {
		I = engine.I;
		T_max = engine.T_max;
		Hv = engine.Hv;
		Hm = engine.Hm;
		C = engine.C;

		for (auto &v : engine.MV) {
			MV.push_back(v);
		}

	}
	~ICE() {}

	void    Start(double T0) {
		std::cout << "start\n";
		T = T0;
		V = 0;

		auto    last = std::chrono::steady_clock::now();

		while (T < T_max) {
			auto    current = std::chrono::steady_clock::now();
			std::chrono::duration<double>    elapsedTime = current - last;
			last = current;
			double   fElapsedTime = elapsedTime.count();
			one_tact(fElapsedTime, T0);
		}
	}
};

class   TestStand {

private:
	ICE     engine;

public:
	TestStand() {}
	TestStand(ICE &e) : engine(e) {}
	~TestStand() {}

	void    Start() {
		double  T0;

		while (true) {
			std::cout << "enter environment temperature or \"stop\" to exite :\n";
			std::string     str;
			std::getline(std::cin, str);

			if (str.compare("stop") == 0) break;

			std::stringstream   s;
			s << str;
			s >> T0;

			auto    start = std::chrono::steady_clock::now();
			engine.Start(T0);
			auto    end = std::chrono::steady_clock::now();
			std::chrono::duration<double>    result = end - start;
			std::cout << "elapsed time : " << result.count() << std::endl;
			std::cout << std::endl;
		}
	}
};

int main() {

	TestStand   ts;

	ts.Start();

	return 0;
}